// Возведение в степень
// function power(n, x){
// 	if(x !== 1){
// 		return n * power(n, x - 1);
// 	}else{
// 		return n;
// 	}
// }
// console.log(power(2, 3));


// Рекурсия
function sumTo(n){
	if(n !== 0){
		return n + sumTo(n - 1);
	}else{
		return n;
	}

}
console.log("***Рекурсия***   - Рекурсия лучше. Потому что всего одна переменная. ");
console.log(sumTo(4));

// Цикл while
var n = 4;
var res = 0;
while (n > 0 ){
	var sumTo = n;
	res += sumTo;
	n--
	// console.log(sumTo);
}
console.log("***Цикл while***");
console.log(res);

// Цикл for
for (var i = 4, result = 0; i > 0; i--) {
	result += i;
}
console.log("***Цикл for***");
console.log(result);